import pytest

from sharebill.db import ShareBillTable

names = ("Lars", "Björn", "Christoph", "Robert")


def test_insert_names():
    c = ShareBillTable(":memory:")
    c.insert_names(names)
    assert c.names() == names


def test_insert_names_unique():
    c = ShareBillTable(":memory:")
    c.insert_names(names)
    with pytest.warns(UserWarning):
        c.insert_names(["Lars"])
    assert c.names() == names


def test_add_transaction():
    c = ShareBillTable(":memory:")
    c.insert_names(names)
    c.add_transaction("Björn", 40)
    t, *_ = c.transactions()
    assert t.paid_by == ("Björn",)
    assert t.paid_for == names
    assert t.amount == 40


def test_equalize():
    c = ShareBillTable(":memory:")
    c.insert_names(names)
    c.add_transaction("Björn", 40)
    c.add_transaction("Lars", 10)
    c.add_transaction("Robert", 5)
    assert c.equalize() == [("Lars", "Björn", 3.75), ("Christoph", "Björn", 13.75), ("Robert", "Björn", 8.75)]


def test_circular():
    c = ShareBillTable(":memory:")
    c.insert_names(names)
    c.add_transaction("Björn", 10, ["Christoph"])
    c.add_transaction("Lars", 10, ["Christoph"])
    c.add_transaction("Björn", 10, ["Lars"])
    assert c.equalize() == [("Christoph", "Björn", 20)]


def test_complex():
    b = (4, 0, 1, 5, 2, 7, 9, 20, 1)
    c = ShareBillTable(":memory:")
    c.insert_names(map(str, range(9)))
    for i, bb in enumerate(b):
        c.add_transaction(str(i), bb)
    assert c.equalize() == [
        ("0", "5", 1.44),
        ("1", "5", 0.11),
        ("1", "6", 3.56),
        ("1", "7", 1.78),
        ("2", "7", 4.44),
        ("3", "7", 0.44),
        ("4", "7", 3.44),
        ("8", "7", 4.44),
    ]
