from sharebill.sharebill import Payment, ShareBill


def test_simple():
    b = (4, 0, 1, 5, 2, 3, 6, 7)
    b = [x - sum(b) / len(b) for x in b]
    f = ShareBill(b)
    s = f.steps()
    assert s == [Payment(*x) for x in [(1, 7, 3.5), (2, 6, 2.5), (4, 3, 1.5), (5, 0, 0.5)]]
