#!/usr/bin/env python3

from collections.abc import Iterator
from dataclasses import dataclass
from typing import Any

from .puzzle import Puzzle


@dataclass(slots=True)
class Payment:
    by: str | int | None = None
    to: str | int | None = None
    amount: float | None = None

    def __iter__(self):
        return iter((self.by, self.to, self.amount))


class ShareBill(Puzzle):
    """
    Calculate the mean amount to be paid, and the amount of payments for a bill of n people

    The amount of payments will be minimal, if breadthFirst search is used (default)

    The input is a n-tuple of amounts which have been paid
    """

    names = None

    def __init__(self, pos: Any = None, decimals: int = 2, epsilon: float = 1e-10):
        """
        pos shall be the amounts which have been paid
        mean should *not* be initialized on the first call of the ctor
        """
        super().__init__(pos)

        self.decimals = decimals
        self.epsilon = epsilon
        # input can be a (sorted) dict, if it is, store the names
        try:
            self.names, self.pos = zip(*self.pos.items())
        except AttributeError:
            pass
        self.valid = [i for i, v in enumerate(self.pos) if abs(v) > self.epsilon]

    def isgoal(self) -> bool:
        # if all values in pos are zero, we have equalized the bill
        return not any(map(lambda x: x > self.epsilon, self.pos))

    def __repr__(self) -> str:
        # take into account the cent factor
        if self.names:
            return repr({n: x for n, x in zip(self.names, self.pos)})
        return repr(tuple(self.pos))

    def __iter__(self) -> Iterator["ShareBill"]:
        """
        Determine all valid moves from a position

        This is somewhat optimized by ignoring "invalid" positons, which are already 0
        This can be further optimized by not looping from i=0...n and j=0...n, but from
        i=0...n and j=i...n
        """
        for i in self.valid:
            for j in self.valid:
                if i == j:
                    continue
                if self.pos[j] > 0 and self.pos[i] < 0:
                    dup = list(self.pos)
                    m, p = dup[i], dup[j]
                    if -m > p:
                        dup[i] += p
                        dup[j] = 0
                    else:
                        dup[i] = 0
                        dup[j] += m
                    yield ShareBill(tuple(dup))

    def steps(self, depthFirst=False) -> list[Payment]:
        """
        Return the steps necessary to equalize the bill
        Returns a list of tuples of payments, and the mean value to be paid

        A tuple consists of (person_to_pay, person_to_receive, amount)
        """

        def diff(x, y):
            return [i - j for i, j in zip(x, y)]

        s = self.solve(depthFirst)

        r = []
        for i in range(len(s) - 1):
            payment = Payment()
            d = diff(s[i].pos, s[i + 1].pos)
            for j, v in enumerate(d):
                if v < 0:
                    payment.by = self.names[j] if self.names else j
                if v > 0:
                    payment.to = self.names[j] if self.names else j
                    payment.amount = round(v, self.decimals)
            r.append(payment)
        return r
