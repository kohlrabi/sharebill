import sqlite3
import warnings
from collections.abc import Iterable, Sequence
from datetime import datetime
from typing import Any

from .sharebill import ShareBill


class Transaction:
    def __init__(
        self,
        date: datetime | float | None = None,
        paid_by: Iterable[str] | None = None,
        paid_for: Iterable[str] | None = None,
        amount: float = 0,
        comment: str = "",
    ):
        if date is None:
            self.date = datetime.now()
        elif isinstance(date, (float, int)):
            self.date = datetime.fromtimestamp(date)
        else:
            self.date = date
        if not paid_by:
            self.paid_by: tuple[str, ...] = tuple()
        else:
            self.paid_by = tuple(paid_by)
        if not paid_for:
            self.paid_for: tuple[str, ...] = tuple()
        else:
            self.paid_for = tuple(paid_for)
        self.amount = amount
        self.comment = comment

    def __repr__(self) -> str:
        ret = f"Transaction(date={self.date!r}, paid_by={self.paid_by!r}, paid_for={self.paid_for!r}, amount={self.amount!r}, comment={self.comment!r})"
        return ret


class ShareBillTable:
    PROV_NAME_TABLE: str = r"CREATE TABLE names(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT UNIQUE)"
    PROV_TRANSACTION_TABLE: str = r"CREATE TABLE transactions(id INTEGER PRIMARY KEY AUTOINCREMENT, date DATETIME, payers INTEGER, receivers INTEGER, amount INTEGER, comment TEXT)"

    def __check_if_provisioned(self) -> bool:
        return True if len(self._con.execute(r"SELECT * FROM sqlite_master WHERE type='table'").fetchall()) else False

    def __init__(self, path: str = ":memory:", names: list[str] | None = None) -> None:
        self._con = sqlite3.connect(path)
        if names is None:
            self._names = []
        else:
            self._names = names
        if not self.__check_if_provisioned():
            self.__provision()
        self.__update_len()

    def __update_len(self) -> None:
        self._len = self._con.execute(r"SELECT MAX(id) from names").fetchone()[0]

    def __len__(self) -> int:
        return self._len

    def __provision(self) -> None:
        self.__create_names_table()
        self.insert_names(self._names, True)
        self.__create_transactions_table()
        self._con.commit()

    def __create_names_table(self) -> None:
        self._con.execute(ShareBillTable.PROV_NAME_TABLE)

    def insert_names(self, names: Iterable[str] | None = None, commit: bool = True) -> None:
        """Insert new names in into database

        Warns if one of the name sis already in the database, and skips that na,e

        Args:
            names (Iterable[str], optional): Iterable of names. Defaults to [].
            commit (bool, optional): Commit the result to the DB after add. Defaults to True.
        """
        if not names:
            return
        dbnames = self.names()
        for name in names:
            if name in dbnames:
                warnings.warn("Name %s already in the database, skipping..." % name)
            else:
                self._con.execute(r"INSERT INTO names(name) VALUES(?)", (name,))
        if commit:
            self._con.commit()
        self.__update_len()

    def __create_transactions_table(self) -> None:
        self._con.execute(ShareBillTable.PROV_TRANSACTION_TABLE)

    def _ids_and_names(self) -> tuple[tuple[int, str], ...]:
        return tuple(self._con.execute(r"SELECT * from names"))

    def names(self) -> tuple[str, ...]:
        return tuple([name for _, name in self._ids_and_names()])

    def ids(self) -> tuple[int, ...]:
        return tuple([id for id, _ in self._ids_and_names()])

    def transactions(self) -> tuple[Transaction, ...]:
        names = {k: v for k, v in self._ids_and_names()}

        transactions = []
        for _, date, paid_by, paid_for, amount, comment in self._con.execute(r"SELECT * from transactions"):
            by_names = [names[bit + 1] for bit in self.__set_bits(paid_by)]
            for_names = [names[bit + 1] for bit in self.__set_bits(paid_for)]
            transactions.append(Transaction(date, by_names, for_names, amount, comment))
        return tuple(transactions)

    def __get_transactions_bitmask(self) -> tuple:
        return tuple(self._con.execute(r"SELECT * from transactions"))

    def __call__(self, *args, **kwargs) -> Any:
        return self._con.execute(*args, **kwargs)

    def add_transaction(
        self,
        paid_by: Sequence[str] | str | None = None,
        amount: float = 0,
        paid_for: Sequence[str] | str | None = None,
        comment: str = "",
        date: datetime | float | None = None,
        commit: bool = True,
    ) -> None:
        """Add a transaction to the DB.

        Args:
            paid_by (Sequence[str] | str, optional): Payment by. Defaults to None.
            amount (float, optional): Amount paid. Defaults to 0.
            paid_for (Sequence[str] | str | None, optional): Payment for. Defaults to None.
            comment (str, optional): Description of the payment. Defaults to "".
            date (datetime | float | None, optional): Date of payment. Defaults to None.
            commit (bool, optional): Commit payment to DB. Defaults to True.
        """
        match date:
            case datetime():
                date = date.timestamp()
            case None:
                date = datetime.now().timestamp()
            case float():
                pass
            case _:
                raise ValueError("Invalid date format")

        if paid_by is None:
            paid_by = tuple()

        if isinstance(paid_by, str):
            paid_by = (paid_by,)

        if isinstance(paid_for, str):
            paid_for = (paid_for,)

        stmt = r"SELECT id from names WHERE name IN ("
        stmt += r",".join(r"?" * len(paid_by))
        stmt += r")"
        ids = tuple([x[0] for x in self._con.execute(stmt, tuple(paid_by)).fetchall()])

        paid_by_ids = 0
        for id in ids:
            paid_by_ids |= 1 << (id - 1)

        if paid_for is None or len(paid_for) == 0:
            # paid for everyone
            stmt = r"SELECT id from names"
            ids = self.ids()
            # ids = tuple([x[0] for x in self._con.execute(stmt)])
        else:
            stmt = r"SELECT id from names WHERE name IN ("
            stmt += r",".join(r"?" * len(paid_for))
            stmt += r")"
            ids = tuple([x[0] for x in self._con.execute(stmt, tuple(paid_for)).fetchall()])
        paid_for_ids = 0
        for id in ids:
            paid_for_ids |= 1 << (id - 1)

        stmt = r"INSERT into transactions(date, payers, receivers, amount, comment) VALUES (?,?,?,?,?)"
        self._con.execute(stmt, (date, paid_by_ids, paid_for_ids, amount, comment))
        if commit:
            self._con.commit()

    @staticmethod
    def __set_bits(x) -> list[int]:
        r = []
        for i in range(64):
            if x == 0:
                break
            if x & 1:
                r.append(i)
            x >>= 1
        return r

    def status(self) -> list[int]:
        """Get the current status of the db.

        Returns:
            list[float]: status
        """
        pos = [0] * len(self)
        for t in self.__get_transactions_bitmask():
            _, __, payers, receivers, amount, ____ = t
            paybits = self.__set_bits(payers)
            recvbits = self.__set_bits(receivers)
            np = len(paybits)
            nr = len(recvbits)
            paid = amount / np
            received = amount / nr
            for p in paybits:
                pos[p] += paid
            for r in recvbits:
                pos[r] -= received
        return pos

    def equalize(self) -> list[tuple[str, str, float]]:
        """Return result of equalizing the bill.

        Returns:
            list[tuple[str, str, float]]: List of payments: Payer, receiver, amount
        """
        pos = self.status()
        solver = ShareBill(pos)
        steps = solver.steps()
        # map solver indices to usernames
        names = {k: v for k, v in self._ids_and_names()}
        mapped = []
        for payer, receiver, amount in steps:
            mapped.append((names[payer + 1], names[receiver + 1], amount))
        return mapped

    def format_equalize(self) -> str:
        """Eqalize the DB and pretty print the result.

        Returns:
            str: pretty printed equalize
        """
        payments = self.equalize()
        s = ""
        for payer, receiver, amount in payments:
            s += f"{payer} pays {amount:.2f} to {receiver}\n"
        return s
